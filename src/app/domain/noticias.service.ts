import { Injectable } from "@angular/core";
import { getJSON, request } from "tns-core-modules/http";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class NoticiasService {
api: string = "https://0297e8adbf91.ngrok.io";

  constructor() {
    this.getDb((db) => {
      console.dir(db);
        db.each("select * from logs",
        (err, fila) => console.log("fila: ", fila),
        (err, totales) => console.log("filas totales: ", totales));
    }, () => console.log("error on getDB"));
  }

  getDb(fnok, fnError) {
    return new sqlite("mi_db_logs", (err, db) => {
      if (err) {
        console.error("Error al abrir el db!", err);
      } else {
        console.log("esta la db abierta: ", db.isOpen() ? "Si" : "No");
        db.execSQL("CREATE TABLE IF NOT EXISTS logs(id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
          .then((id) => {
            console.log("CREATE TABLE OK");
            fnok(db);
          }, (error) => {
            console.log("CREATE TABLE ERROR", error);
            fnError(error);
          });
      }
    });
  }

  agregar(s: string) {
    return request({
      url: this.api + "/favs",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify({
        nuevo: s
      })
    });
  }

  favs() {
    return getJSON(this.api + "/favs");
  }

  buscar(s: string) {
    this.getDb((db) => {
      db.execSQL("insert into logs (texto) values (?)", [s],
        (err, id) => console.log("nuevo id: ", id));
    }, () => console.log("error on getDb"));

    return getJSON(this.api + "get?q=" + s);
  }

}
