import { Component, OnInit } from "@angular/core";
import * as camera from "nativescript-camera";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Image } from "tns-core-modules/ui/image";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTap(): void{
      camera.requestPermissions().then(
        function success() {
          const options = { width: 300, height: 300, keepAspectRation: false, saveToGallery: true };
          camera.takePicture(options).
            then((imageAssets) => {
              console.log("Tamaño: " + imageAssets.options.width + "x" + imageAssets.options.height);
              console.log("keepAspectRation: " +imageAssets.options.keepAspectRatio);
              console.log("Foto guardada!");
            }).catch((err) => {
              console.log("Error ->" + err.message);
            });
        },
        function failure() {
          console.log("permiso de camara no aceptado por el usuario")
        }
      );
    }
}
