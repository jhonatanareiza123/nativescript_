import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import *as SocialShare from "nativescript-social-share";
import * as app from "tns-core-modules/application";
import { Color, View } from "tns-core-modules/ui/core/view/view";
import { NoticiasService } from "../domain/noticias.service";
import * as Toast from "nativescript-toast";
import { Noticia, NoticiasState, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { AppState } from "../app.module";

@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
    resultados: Array<string>;
    @ViewChild("layout") layout: ElementRef;

    constructor(
      private noticias: NoticiasService,
      private store: Store<AppStore>
      ) {

    }

    ngOnInit(): void {
      this.store.select((state) => state.noticias.sugerida)
        .subscribe((data) => {
        const f = data;
        if (f != null) {
          Toast.show({text: "sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
        }
      });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
      this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    onLongPress(s): void{
      console.log(s);
      SocialShare.shareText(s, "Asunto: compartido desde el cursor");
    }

    buscarAhora(s: string) {
      console.dir("buscarAhora" + s);
      this.noticias.buscar(s).then((r: any) => {
        console.log("resultados buscarAhora: " +JSON.stringify(r));
        this.resultados = r;
      }, (e) => {
        console.log("error buscarAhora " + e);
        Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT});
      })
    }
}
